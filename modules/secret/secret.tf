resource "aws_secretsmanager_secret" "bitbucket_secrets" {
  name = var.secret_name
}

resource "aws_secretsmanager_secret_version" "bitbucket_secrets" {
  secret_id     = aws_secretsmanager_secret.bitbucket_secrets.id
  secret_string = jsonencode({username = var.username,password = var.password})
}

resource "aws_iam_role_policy" "secret_policy" {
  count = 1
  role = aws_iam_role.ds_user_role.id
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "OpenNotebookUsingSecretPermission",
            "Effect": "Allow",
            "Action": "secretsmanager:GetSecretValue",
            "Resource": "${aws_secretsmanager_secret.bitbucket_secrets.arn}"
        }
    ]
}
EOF
}