#---------------get user arn
data "aws_iam_user" "user" {
  user_name = var.aws_iam_user
}

#Creation of user role
resource "aws_iam_role" "ds_user_role" {
  name = "user_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
            "AWS": "${data.aws_iam_user.user.arn}"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

