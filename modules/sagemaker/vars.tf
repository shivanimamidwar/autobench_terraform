variable "sagemaker_enable" {
	default = true
}

variable "notebook_instance_name" {
  default	= "autobench"
}

variable "instance_type" {
  default = "ml.m5.xlarge"   
}

# Services
variable "athena_service_enable" {
	default = false
}

variable "glue_service_enable" {
  default = false
}

variable "aws_iam_user" {
  # default = "shivani.mamidwar@ezest.in"
}

variable "s3_enable" {
  default = "false"  
}

variable "caspian_side_bucket" {
  default = "false"
}

variable "general_purpose_bucket_arn"{
  default = "false" 
}  


variable "bucket_caspian" {
  default = "ds-user-101"
}

variable "bucket_name" {
  default = "ds-user-102"
}


