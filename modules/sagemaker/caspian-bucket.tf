
# Bucket that pushes data to Caspian bucket
resource "aws_s3_bucket" "cp" {
  bucket = var.bucket_caspian
  acl = "private"
  count = 1
  # acl = "private"
}

resource "aws_iam_role" "s3_role" {
  name = "s3_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "s3.amazonaws.com"
        }
      },
    ]
  })
}



resource "aws_iam_role_policy" "s3_cp_bucket_policy" {
  count = var.s3_enable == true ? 1 : 0
  role = aws_iam_role.s3_role.id
  policy = <<EOF
   
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListAllMyBuckets",
        "s3:GetBucketLocation"
      ],
      "Resource": "arn:aws:s3:::*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket"
      ],
      "Resource": [
        "${aws_s3_bucket.cp[count.index].arn}"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject"
      ],
      "Resource": [
        "${aws_s3_bucket.cp[count.index].arn}/*"
      ]
    }
  ]
}
EOF
}