resource "aws_s3_bucket" "gp" {
  bucket = var.bucket_name
  acl = "private"
  count = 1
}

# GP bucket policy
resource "aws_iam_role_policy" "s3_gp_bucket_policy" {
  count = var.s3_enable == true ? 1 : 0
  role = aws_iam_role.s3_role.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListAllMyBuckets",
        "s3:GetBucketLocation"
      ],
      "Resource": "arn:aws:s3:::*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket"
      ],
      "Resource": [
        "${aws_s3_bucket.gp[count.index].arn}"
      ],
      "Condition": {"StringEquals" : {"s3:prefix":["","Athena/"],"s3:delimiter":["/"]}}
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket"
      ],
      "Resource": [
        "${aws_s3_bucket.gp[count.index].arn}"
      ],
      "Condition": {"StringLike" : {"s3:prefix":["Athena/*"]}}
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject"
      ],
      "Resource": [
        "${aws_s3_bucket.gp[count.index].arn}/Athena/*"
      ]
    }
  ]
}
EOF
}
