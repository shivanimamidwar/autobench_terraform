provider "aws" {
  region = var.region

}

terraform {
  required_providers {
    bitbucket = {
      source  = "zahiar/bitbucket"
    }
  }
}