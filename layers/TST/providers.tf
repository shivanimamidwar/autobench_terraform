provider "aws" {
  region = "us-east-2"

}

terraform {
  required_providers {
    bitbucket = {
      source  = "zahiar/bitbucket"
    }
  }
}