module "bitbucket" {
  source = "../../modules/bitbucket"
  repo_name   = var.repo_name
  project_key = var.project_key
  workspace   = var.workspace 
}

module "lambda" {
  source = "../../modules/lambda"
  function_name = "ca-dse-${var.function_name}-wb-${var.workbench_name}-fn"
}

module "sagemaker" {
  source = "../../modules/sagemaker"
  instance_type = var.instance_type
  notebook_instance_name = "ca-dse-${var.notebook_instance_name}-wb-${var.workbench_name}-noteb"
  athena_service_enable = "ca-dse-${var.stage}-wb-${var.workbench_name}-iam-at"
  glue_service_enable = "ca-dse-${var.stage}-wb-${var.workbench_name}-iam-gs"
  bucket_caspian = "ca-dse-${var.bucket_name}-wb-${var.workbench_name}-gen-p-b"
  bucket_ganeral = "ca-dse-${var.bucket}-wb-${var.workbench_name}-casp-b"
  caspian_side_bucket = var.caspian_side_bucket
  general_purpose_bucket_arn = var.general_purpose_bucket_arn 
  s3_enable = "ca-dse-${var.stage}-wb-${var.workbench_name}-iam-s3"
  
}

module "secret" {
  source = "../../modules/secret"
  username = var.username
  password = var.password
  secret_name = "ca-dse-${var.secret_name}-wb-${var.workbench_name}-iam-at"
} 
 
  
  



  
  


  
  
  




  



  
  



